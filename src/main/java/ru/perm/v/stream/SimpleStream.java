package ru.perm.v.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleStream {

  private SimpleStream() {
  }

  /**
   * Кол-во слов в строке s с длиной больше maxLength
   *
   * @param s - строка
   * @param maxLength - макс.длина
   * @return - кол-во слов с макс.длиной
   */
  public static long getCountLarger(String s, int maxLength) {
    List<String> words = Arrays.asList(s.split("[\\P{L}]+"));
    return words.stream().filter(w -> w.length() > maxLength).count();
  }

  /**
   * Создание потока из массива
   *
   */
  public static List<String> streamFromArray(String... s) {
    Stream<String> stream = Stream.of(s);
    // Stream.of("aa","bb","cc")
    return stream.collect(Collectors.toList());
  }

  /**
   * Пример фильтра
   *
   * @param maxLength - макс.длина
   * @param s         - список тесируемых строк
   * @return - кол-во строк с длиной больше maxLength
   */
  public static long streamFilter(int maxLength, String... s) {
    return Stream.of(s).filter(w -> w.length() > maxLength).count();
  }

  /**
   * Пример map
   *
   */
  public static List<String> streamMap(String... strings) {
    return Stream.of(strings).map(String::toUpperCase)
        .collect(Collectors.toList());
  }

  /**
   * Генерация и ограничение потока
   */
  public static long streamLimit(long max) {
    return Stream.generate(Math::random).limit(max).count();
  }

  /**
   * Пример сведения. Получение макс.значения.
   */
  public static Integer getMax(Integer... ints) {
    return Stream.of(ints).max(Integer::compareTo).orElse(0);
  }

  /**
   * Пример Optional.orElse, ofNullable
   */
  public static String getNotNull(String str) {
    return Optional.ofNullable(str).orElse("");
  }

  /**
   * Пример reduce
   */
  public static Integer checkReduce(Integer... ints) {
    return Stream.of(ints).reduce(Integer::sum).orElse(0);
  }

  /**
   * Еще пример reduce
   */
  public static Integer checkReduce1(Integer... ints) {
    return Stream.of(ints).reduce(Integer::sum).orElse(0);
  }

  /**
   * Использование reduce с накоплением
   */
  public static Integer checkReduceSum(String... strs) {
    return Stream.of(strs).reduce(0,
        (total, word) -> total + word.length(),
        Integer::sum);
  }

  /**
   * Альтернатива использования reduce с накоплением
   */
  public static Integer checkSum(String... strs) {
    return Stream.of(strs).mapToInt(String::length).sum();
  }

  /**
   * Альтернатива 2 использования reduce с накоплением
   */
  public static Integer checkSum1(String... strs) {
    return Stream.of(strs).map(String::length).reduce(Integer::sum)
        .orElse(0);
  }

  /**
   * Преобразование в массив
   */
  public static String[] checkToArray(String... strs) {
    return Stream.of(strs).toArray(String[]::new);
  }

  /**
   * collect(Collectors.toList())
   */
  public static List<String> checkToList(String... strs) {
    return Stream.of(strs).collect(Collectors.toList());
  }

  /**
   * collect(Collectors.toSet())
   */
  public static Set<String> checkToSet(String... strs) {
    return Stream.of(strs).collect(Collectors.toSet());
  }

  /**
   * collect(Настроить коллекцию)
   */
  public static TreeSet<String> checkToCustom(String... strs) {
    return Stream.of(strs)
        .collect(Collectors.toCollection(TreeSet::new));
  }

  /**
   * collect(join)
   */
  public static String checkJoining(String delimeter, String... strs) {
    return Stream.of(strs).collect(Collectors.joining(delimeter));
  }

  /**
   * collect(summarizing)
   */
  public static long checkSummarizing(String... strs) {
    return Stream.of(strs)
        .collect(Collectors.summarizingInt(String::length)).getSum();
  }

  /**
   * Проверка peek
   */
  public static List<String> peek(String... words) {
    ArrayList<String> ret = new ArrayList<>();
    // count добавлен для того чтобы поток запустился
    Stream.of(words).peek(ret::add).count();
    return ret;
  }

  /**
   * Проверка peek
   */
  public static List<String> forEach(String... words) {
    ArrayList<String> ret = new ArrayList<>();
    Stream.of(words).forEach(ret::add);
    return ret;
  }
}
