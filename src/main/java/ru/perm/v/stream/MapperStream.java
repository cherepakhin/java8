package ru.perm.v.stream;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapperStream {

  private MapperStream() {
  }

  static Map<Integer, String> mapIdName(Person... persons) {
    return Stream.of(persons)
        .collect(Collectors.toMap(Person::getId, Person::getName));
  }

  static Map<Integer, Person> mapIdPerson(Person... persons) {
    return Stream.of(persons)
        .collect(Collectors.toMap(Person::getId, Function.identity()));
  }
}
