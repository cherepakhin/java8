package ru.perm.v.generic;

public class EntityName extends AEntityName {

    public EntityName(Integer id) {
        super(id);
    }

    @Override
    public String toString() {
        return "EntityName{" +
            "name='" + name + '\'' +
            ", id=" + id +
            '}';
    }
}

