package ru.perm.v.generic;

public abstract class AEntity {
    Integer id=0;

    public AEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
