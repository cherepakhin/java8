package ru.perm.v.generic;

public class Entity extends AEntity {

    public Entity(Integer id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Entity{" +
            "id=" + id +
            '}';
    }
}
