package ru.perm.v.generic;

public class AEntityName extends AEntity {
    String name;

    public AEntityName(Integer id) {
        super(id);
        name = "name:"+id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
