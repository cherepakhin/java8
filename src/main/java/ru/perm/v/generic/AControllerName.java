package ru.perm.v.generic;

public class AControllerName<E extends AEntityName> extends AController<E>
    implements IControllerName<E> {

    @Override
    public E getByName(E e) {
        return e;
    }

//    public E getById(E e) {
//        return super.getById(e);
//    }
}
