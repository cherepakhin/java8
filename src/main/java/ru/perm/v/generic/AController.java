package ru.perm.v.generic;

public class AController<E extends AEntity> implements IController<E> {

    @Override
    public E getById(E e) {
        System.out.println("getById:"+e);
        return e;
    }
}
