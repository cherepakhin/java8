package ru.perm.v.generic;

public interface IController<E extends AEntity> {
    E getById(E e);
}
