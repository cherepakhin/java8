package ru.perm.v.generic;

public interface IControllerName<E extends AEntityName> extends IController<E>{
    E getByName(E e);
}
