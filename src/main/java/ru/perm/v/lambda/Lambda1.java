package ru.perm.v.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.IntBinaryOperator;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lambda1 {

  public static Optional<Double> inverse(Double x) {
    return x == 0 ? Optional.empty() : Optional.of(1 / x);
  }

  public static Optional<Double> squareRoot(Double x) {
    return x < 0 ? Optional.empty() : Optional.of(Math.sqrt(x));
  }

  public static Stream<String> characterStream(String s) {
    ArrayList<String> result = new ArrayList<String>();
    for (Character c : s.toCharArray()) {
      result.add(c.toString());
    }
    return result.stream();
  }

  public List<String> sortList(String[] arr) {
    List<String> list = Arrays.asList(arr);
    Collections
        .sort(list, (s1, s2) -> Integer.compare(s1.length(), s2.length()));
    return list;
  }

  public String[] sortArr(String[] arr) {
    Arrays.sort(arr, (s1, s2) -> Integer.compare(s1.length(), s2.length()));
    return arr;
  }

  /**
   * Использование статического метода интерфейса
   *
   * @param arr
   * @return
   */
  public String[] sortComparatorArr(String[] arr) {
    Arrays.sort(arr, Comparator.comparing(String::length));
    return arr;
  }

  IntBinaryOperator getOperation() {
    IntBinaryOperator intBinaryOperator = (x, y) -> x + y;
    return intBinaryOperator;
  }

  void applyConsumer(IntConsumer action) {
    action.accept(22);
  }

  List<String> linkToMethod(List<String> list) {
    Collections.sort(list, String::compareToIgnoreCase);
    return list;
  }

  List<Integer> linkToConstructor(List<String> strings) {
    List<Integer> ints = strings.stream().map(Integer::new)
        .collect(Collectors.toList());
    return ints;
  }
}
