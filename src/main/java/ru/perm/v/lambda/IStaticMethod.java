package ru.perm.v.lambda;

public interface IStaticMethod {

  static String getName() {
    return "static method";
  }
}
