package ru.perm.v.lambda.default_method;

public class Person implements IPerson {

  @Override
  public int getId() {
    return 1;
  }
}
