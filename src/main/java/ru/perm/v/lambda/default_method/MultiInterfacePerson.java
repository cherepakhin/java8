package ru.perm.v.lambda.default_method;

public class MultiInterfacePerson implements IPerson, INamed {

  @Override
  public int getId() {
    return 2;
  }

  @Override
  public String getName() {
    return INamed.super.getName();
  }

}
