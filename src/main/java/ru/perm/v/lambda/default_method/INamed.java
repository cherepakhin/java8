package ru.perm.v.lambda.default_method;

public interface INamed {

  default String getName() {
    return "VASI";
  }

}
