package ru.perm.v.lambda.default_method;

public interface IPerson {

  int getId();

  default String getName() {
    return "vasi";
  }

}
