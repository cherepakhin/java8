package ru.perm.v.base;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayListTestSimple {

  @Test
  public void copyTest() {
    int[] arr = new int[]{1, 2, 3};
    int[] arrOther = Arrays.copyOf(arr, 2);
    assertArrayEquals(arrOther, new int[]{1, 2});
  }

  @Test
  public void initArrayListFromArray() {
    String[] arr = new String[]{"aa", "bb"};
    ArrayList<String> arrList = new ArrayList<String>(Arrays.asList(arr));
    assertEquals(arrList.get(0), "aa");
  }

  @Test
  public void initArrayFromArrayList() {
    ArrayList<String> arrList = new ArrayList<String>();
    arrList.add("aa");
    arrList.add("bb");
    String[] arr = arrList.toArray(new String[0]);
    assertEquals(arr[0], "aa");
    assertEquals(arr[1], "bb");
  }

  @Test
  public void fillTest() {
    int[] numbers = new int[2];
    Arrays.fill(numbers, 1);
    assertTrue(numbers[0] == 1);
    assertTrue(numbers[1] == 1);

    ArrayList<String> strings = new ArrayList<>();
    strings.add("aaa");
    Collections.fill(strings, "1");
    assertEquals(strings.get(0), "1");
  }

  int[] getVarInt(int... vars) {
    int[] ret = new int[vars.length];
    for (int i = 0; i < vars.length; i++) {
      ret[i] = vars[i];
    }
    return ret;
  }

  /**
   * Вызов метода с переменным количеством переменных
   */
  @Test
  public void varArgsTest() {
    int[] arr = getVarInt(1, 2, 3);
    assertArrayEquals(arr, new int[]{1, 2, 3});
  }
}
