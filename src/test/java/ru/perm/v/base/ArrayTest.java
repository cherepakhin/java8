package ru.perm.v.base;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayTest {

  @Test
  public void initTest() {
    String[] arr = new String[10];
    assertNull(arr[0]);

    int[] arrInt = new int[10];
    assertTrue(arrInt[0] == 0);

    arrInt = new int[]{1, 2, 3};
    assertTrue(arrInt[0] == 1);

    int[] arrIntFill = {1, 2, 3};
    assertTrue(arrIntFill[0] == 1);

    boolean[] arrBoolean = new boolean[10];
    assertFalse(arrBoolean[0]);

  }
}
