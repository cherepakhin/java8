package ru.perm.v.base;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringTest {

  @Test
  public void testJoin() {
    String actual = String.join(",", "A", "B", "C");
    assertEquals(actual, "A,B,C");
  }

  @Test
  public void testFormat() {
    String s = String.format("%s", "STRING");
    assertEquals(s, "STRING");

    s = String.format("%d", 5);
    assertEquals(s, "5");

    s = String.format("%.2f", 5.2);
    System.out.println(s);
    assertEquals(s, "5,20");

    s = String.format("%6.2f", 5.2); // 6- общая длина строки
    System.out.println(s);
    assertEquals(s, "  5,20");

  }
}
