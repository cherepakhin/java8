package ru.perm.v.base;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NullTest {

    class A {
        String str;

        public String getStr() {
            return str;
        }

        public void setStr(String s) {
            this.str = s;
        }
    }

    @Test
    public void nullEqNull() {
        A a = new A();

        assert null == null; // TRUE
        assert a.getStr() == null; // TRUE (s = null)
    }

    @Test
    public void equasNPE() {
        A a = new A();
        Assertions.assertThrows(NullPointerException.class, () -> a.getStr().equals("")); // NPE, s==null
    }

    @Test
    public void compileOkButRunError() {
        Integer i_am_null = null;
        // При компиляции ошибки НЕТ, а при выполнении NPE
        // т.к. i_am_null = null
        Assertions.assertThrows(NullPointerException.class, () -> {
            int i = i_am_null;
        });
    }
}
