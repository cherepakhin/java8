package ru.perm.v.base;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FinalTest {

  @Test
  public void testFinalConst() {
    assertEquals(Final.CONST, 100);
  }
}