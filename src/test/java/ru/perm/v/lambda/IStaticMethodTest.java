package ru.perm.v.lambda;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IStaticMethodTest {

  @Test
  public void getName() {
    assertEquals(IStaticMethod.getName(), "static method");
  }
}