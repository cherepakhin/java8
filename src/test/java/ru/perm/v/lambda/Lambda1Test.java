package ru.perm.v.lambda;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Lambda1Test {


    @Test
    public void sortList() {
        Lambda1 lambda1 = new Lambda1();
        List<String> actual = lambda1.sortList(new String[]{"22", "1"});
        String[] expected = new String[]{"1", "22"};
        assertEquals(Arrays.asList(expected), actual);
    }

    @Test
    public void sortArr() {
        Lambda1 lambda1 = new Lambda1();
        String[] actual = lambda1.sortArr(new String[]{"22", "1"});
        String[] expected = new String[]{"1", "22"};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void sortComparatorArr() {
        Lambda1 lambda1 = new Lambda1();
        String[] actual = lambda1.sortComparatorArr(new String[]{"22", "1"});
        String[] expected = new String[]{"1", "22"};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void getOperation() {
        Lambda1 lambda1 = new Lambda1();
        int sum = lambda1.getOperation().applyAsInt(4, 5);
        assertEquals(9, sum);
    }


    @Test
    public void applyConsumer() {
        Lambda1 lambda1 = new Lambda1();
        lambda1.applyConsumer((i) -> System.out.println(i));
    }

    @Test
    public void linkToMethod() {
        List<String> list = new ArrayList<String>() {
            {
                add("BB");
                add("bb");
                add("AA");
                add("aa");
            }
        };

        List<String> expected = new ArrayList<String>() {
            {
                add("AA");
                add("aa");
                add("BB");
                add("bb");
            }
        };

        Lambda1 lambda1 = new Lambda1();
        List<String> actual = lambda1.linkToMethod(list);
        assertEquals(actual, expected);
    }

    @Test
    public void linkToConstructor() {
        List<String> list = new ArrayList<String>() {
            {
                add("1");
                add("2");
            }
        };

        List<Integer> expected = new ArrayList<Integer>() {
            {
                add(1);
                add(2);
            }
        };

        Lambda1 lambda1 = new Lambda1();
        List<Integer> actual = lambda1.linkToConstructor(list);
        assertEquals(actual, expected);
    }

    @Test
    public void optionalFlatMap() {
        Double result =
                Optional.of(4.0).flatMap(Lambda1::inverse)
                        .flatMap(Lambda1::squareRoot).orElse(0.0);
        assertEquals(Double.valueOf(0.5), result);
    }

    @Test
    public void optionalFlatMapNotPresent() {
        Optional<Double> result =
                Optional.of(-4.0).flatMap(Lambda1::inverse)
                        .flatMap(Lambda1::squareRoot);
        assertEquals(Optional.empty(), result);
    }

    @Test
    public void characterStreamTest() {
        Stream<String> expected = Lambda1
                .characterStream("abc");
        assertArrayEquals("a,b,c".split(","), expected.toArray());
    }

    @Test
    public void characterStreamsTest() {
        Stream<String> actual =
                Stream.of("abc", "def").flatMap(Lambda1::characterStream);
        Stream<String> expected = Arrays.stream("a,b,c,d,e,f".split(","));
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void testReduce() {
        Stream<String> words = Stream.of("aa", "bbb", "cccc");
        Integer len = words
                .reduce(
                        0,
                        (total, word) -> total + word.length(),
                        (total1, total2) -> total1 + total2
                );
        assertEquals(9, len.intValue());
    }

    @Test
    public void sumTest() {
        Integer length = Stream.of("abc", "def").map(String::length)
                .reduce(0, Integer::sum);
        assertEquals(6, length.intValue());
        length = Stream.of("abc", "de").map(String::length)
                .reduce(Integer::sum).orElse(0);
        assertEquals(5, length.intValue());
    }

    @Test
    public void collectToSet() {
        HashSet<String> hash = Arrays.stream("a,b,c,d,e,f".split(","))
                .collect(HashSet::new, HashSet::add, HashSet::addAll);
        HashSet<String> expect = new HashSet<String>();
        expect.addAll(Arrays.asList("a,b,c,d,e,f".split(",")));
        assertArrayEquals(expect.toArray(), hash.toArray());
    }

    //    @Test
//    public void getBiFunctionSort() {
//        List<String> list = new ArrayList<String>() {
//            {
//                add("111");
//                add("2");
//            }
//        };
//        BiFunction<String, String, Integer> comparator = lambda1
//            .getBiFunctionSort();
//
//
//        List<String> actual = Collections.sort(list, comparator::apply);
//    }
}