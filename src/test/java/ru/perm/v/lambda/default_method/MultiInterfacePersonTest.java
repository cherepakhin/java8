package ru.perm.v.lambda.default_method;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiInterfacePersonTest {

  @Test
  public void getName() {
    MultiInterfacePerson p = new MultiInterfacePerson();
    assertEquals(p.getName(), "VASI");
  }
}