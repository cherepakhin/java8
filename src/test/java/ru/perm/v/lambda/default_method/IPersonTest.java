package ru.perm.v.lambda.default_method;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IPersonTest {

  @Test
  public void getName() {
    Person p = new Person();
    assertEquals(p.getName(), "vasi");
  }
}