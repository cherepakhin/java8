package ru.perm.v.time;

import org.junit.jupiter.api.Test;

import java.time.LocalTime;

public class CheckerLocalTimeTest {

  @Test
  public void exampleTime() {
    System.out.println(LocalTime.of(10, 30));
    System.out.println(LocalTime.of(10, 30, 58));
    System.out.println(LocalTime.now());
  }
}
