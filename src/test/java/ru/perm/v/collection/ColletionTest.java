package ru.perm.v.collection;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ColletionTest {

  @Test
  public void collectToSet1() {
    Set<String> hash = Arrays.stream("a,b,c,d,e,a".split(","))
        .collect(Collectors.toSet());
    Set<String> expect = new HashSet<String>();
    expect.addAll(Arrays.asList("a,b,c,d,e,a".split(",")));
    assertArrayEquals(expect.toArray(), hash.toArray());
    assertEquals(5, hash.size());
  }

  @Test
  public void collectJoining() {
    String joiningString = Arrays.stream("a,b,c,d,e".split(","))
        .collect(Collectors.joining("-"));
    assertEquals("a-b-c-d-e", joiningString);
  }

  @Test
  public void collectSummarizing() {
    IntSummaryStatistics len = Arrays.stream("a,b,c,d,e".split(","))
        .collect(Collectors.summarizingInt(String::length));
    assertEquals(5, len.getSum());
  }

}
