package ru.perm.v.stream;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Накопление данных в отображениях
 */
public class MapperStreamTest {

  @Test
  public void mapIdName() {
    Map<Integer, String> actual = MapperStream.mapIdName(
        new Person(1, "one"),
        new Person(2, "two"));
    Map<Integer, String> expected = new HashMap<Integer, String>();
    expected.put(1, "one");
    expected.put(2, "two");
    assertEquals(expected, actual);
  }

  @Test
  public void mapIdPerson() {
    Map<Integer, Person> actual = MapperStream.mapIdPerson(
        new Person(1, "one"),
        new Person(2, "two"));
    Map<Integer, Person> expected = new HashMap<Integer, Person>();
    expected.put(1, new Person(1, "one"));
    expected.put(2, new Person(2, "two"));
    assertEquals(expected, actual);
  }

  @Test
  public void comparatorTreeSet() {
    Person p1 = new Person(1, "one");
    Person p2 = new Person(2, "two");
    Person p11 = new Person(1, "ONE");
    TreeMap<Integer, Set<Person>> actual = Stream.of(p1, p2, p11)
        .collect(Collectors.toMap(
            Person::getId,
            Collections::singleton,
            (a, b) -> {
              Set<Person> res = new HashSet<Person>(a);
              res.addAll(b);
              return res;
            },
//            TreeMap::new // Простой вариант
            () -> new TreeMap<Integer, Set<Person>>(
                (o1, o2) -> o2.compareTo(o1))
            )
        );
    actual.entrySet().forEach(System.out::println);
    assertEquals(2, actual.size());
    assertEquals(2, actual.get(1).size());
    assertTrue(actual.get(1).containsAll(Arrays.asList(p1, p11)));
    assertTrue(actual.get(2).contains(p2));
  }
}