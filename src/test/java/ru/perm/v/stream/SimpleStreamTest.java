package ru.perm.v.stream;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleStreamTest {


  @Test
  public void getCountLarger() {
    assertEquals(1, SimpleStream.getCountLarger("a aaa bbbb bb", 3));
  }

  @Test
  public void streamFromArray() {
    List<String> actual = SimpleStream.streamFromArray("aa", "bb");
    ArrayList<String> expected = new ArrayList<String>() {
      {
        add("aa");
        add("bb");
      }
    };
    assertEquals(expected, actual);
  }

  @Test
  public void streamFilter() {
    long count = SimpleStream.streamFilter(2, "aaaa", "bb", "ccc");
    assertEquals(2, count);
  }

  @Test
  public void streamMap() {
    List<String> actual = SimpleStream.streamMap("aa", "bb");
    ArrayList<String> expected = new ArrayList<String>() {
      {
        add("AA");
        add("BB");
      }
    };
    assertEquals(expected, actual);
  }

  @Test
  public void streamLimit() {
    assertEquals(100, SimpleStream.streamLimit(100));
  }

  @Test
  public void getMax() {
    Integer max = SimpleStream.getMax(1, 2, 3, 4);
    assertEquals(4, (int) max);
  }


  @Test
  public void getNotNull() {
    assertEquals(SimpleStream.getNotNull(null), "");
  }

  @Test
  public void reduce() {
    Integer sum = SimpleStream.checkReduce(1, 2, 3);
    assertEquals(6, (int) sum);
  }

  @Test
  public void reduce1() {
    Integer sum = SimpleStream.checkReduce1(1, 2, 3);
    assertEquals(6, (int) sum);
  }

  @Test
  public void checkReduceSum() {
    Integer sum = SimpleStream.checkReduceSum("a", "bb", "ccc");
    assertEquals(6, (int) sum);
  }

  @Test
  public void checkSum() {
    Integer sum = SimpleStream.checkSum("a", "bb", "ccc");
    assertEquals(6, (int) sum);
  }

  @Test
  public void checkSum1() {
    Integer sum = SimpleStream.checkSum1("a", "bb", "ccc");
    assertEquals(6, (int) sum);
  }

  @Test
  public void checkToArray() {
    String[] actual = SimpleStream.checkToArray("a", "b", "c");
//        System.out.println(actual[0]);
    assertArrayEquals(actual, new String[]{"a", "b", "c"});
  }

  @Test
  public void checkToList() {
    List<String> actual = SimpleStream.checkToList("aa", "bb");
    ArrayList<String> expected = new ArrayList<String>() {
      {
        add("aa");
        add("bb");
      }
    };
    assertEquals(expected, actual);
  }

  @Test
  public void checkToSet() {
    Set<String> actual = SimpleStream.checkToSet("aa", "bb");
    Set<String> expected = new HashSet<String>() {
      {
        add("aa");
        add("bb");
      }
    };
    assertEquals(expected, actual);
  }

  @Test
  public void checkToCustom() {
    TreeSet<String> actual = SimpleStream.checkToCustom("b", "a");
    TreeSet<String> expected = new TreeSet<String>() {
      {
        add("a");
        add("b");
      }
    };
    assertEquals(expected, actual);
  }

  @Test
  public void checkJoining() {
    String actual = SimpleStream.checkJoining(";", "a", "b");
    assertEquals(actual, "a;b");
  }

  @Test
  public void checkSummarizing() {
    long actual = SimpleStream.checkSummarizing("a", "bb");
    assertEquals(3, actual);
  }

  @Test
  @Ignore
  public void peek() {
    List<String> actual = SimpleStream.peek("aa", "bb");
    System.out.println(actual);
    List<String> expected = Arrays.asList("aa", "bb");
    System.out.println(expected);
    assertEquals(expected, actual);
  }

  @Test
  public void forEach() {
    List<String> actual = SimpleStream.forEach("aa", "bb");
    ArrayList<String> expected = new ArrayList<String>() {
      {
        add("aa");
        add("bb");
      }
    };
    assertEquals(expected, actual);
  }
}