package ru.perm.v.json;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;


public class Sale implements Serializable, Comparable<Sale> {

    String day = "";
    Integer sales = 0;

    public Sale(LocalDate day, Integer sales) {
        this.day = day.format(DateTimeFormatter.ISO_DATE);
        this.sales = sales;
    }

    public Sale(Map.Entry<LocalDate, Integer> entry) {
        this.day = entry.getKey().format(DateTimeFormatter.ISO_DATE);
        this.sales = entry.getValue();
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    @Override
    public int compareTo(Sale sale) {
        return day.compareTo(sale.day);
    }
}
