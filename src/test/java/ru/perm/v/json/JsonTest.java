package ru.perm.v.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonTest {

    final ObjectMapper objectMapper = new ObjectMapper();
    final Long ID = 0L;
    final String NAME = "vasi";
    final UUID GUID = UUID.fromString("142d5ae7-99c7-4496-9ebd-44d9b3a19714");

    @Test
    public void simple() throws IOException {
        String json = String.format("{ \"id\" : %d, \"name\" : \"%s\" }", ID, NAME);
        PersonJson personJson = objectMapper.readValue(json,
                PersonJson.class);
        assertEquals(ID, personJson.getId());
        assertEquals(NAME, personJson.getName());
        assertEquals("", personJson.getAddress());
    }

    @Test
    public void ignoreUnknown() throws IOException {
        String json = String.format(""
                + "{ \"id\" : %d, "
                + "\"name\" : \"%s\","
                + "\"random_field\": 2 "
                + "}", ID, NAME);
        PersonJson personJson = objectMapper.readValue(json,
                PersonJson.class);
        assertEquals(ID, personJson.getId());
        assertEquals(NAME, personJson.getName());
        assertEquals("", personJson.getAddress());
    }

    @Test
    public void testGUID() throws IOException {
        String json = String.format(""
                + "{ \"id\" : %d, "
                + "\"name\" : \"%s\","
                + "\"guid\": \"%s\" "
                + "}", ID, NAME, GUID);
        PersonJson personJson = objectMapper.readValue(json,
                PersonJson.class);
        assertEquals(GUID, personJson.getGuid());
    }

    @Test
    public void mapToJson() throws JsonProcessingException {
        HashMap<LocalDate, Integer> map = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            map.put(LocalDate.now().plusDays(i), i);
        }
        String s = objectMapper.writeValueAsString(map);
        System.out.println(s);

        List<Sale> sales = map.entrySet().stream()
                .map(Sale::new).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.println(objectMapper.writeValueAsString(sales));

        LocalDate today = LocalDate.now();
        LocalDate last = today.withDayOfMonth(1);
        System.out.println(last.minusDays(1));
        System.out.println(last.minusMonths(1).minusDays(1));
    }
}
