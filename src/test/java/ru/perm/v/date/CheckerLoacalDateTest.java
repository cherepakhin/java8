package ru.perm.v.date;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class CheckerLoacalDateTest {

  @Test
  public void simple() {
    LocalDate nowDate = LocalDate.now();
    System.out.println(nowDate); // 2019-07-21

    LocalDate birthDay = LocalDate.of(1967, Month.MARCH, 8);
    System.out.println(birthDay);

    LocalDate tomorrow = nowDate.plusDays(1);
    System.out.println(tomorrow);

    LocalDate nextYear = nowDate.plusYears(1);
    System.out.println(nextYear);

    LocalDate nextMonth = nowDate.plusMonths(1);
    System.out.println(nextMonth);

    System.out.println(birthDay.getDayOfMonth());
    System.out.println(birthDay.getMonth()); // MARCH
    System.out.println(birthDay.getMonthValue()); // 3
    System.out.println(birthDay.getDayOfYear()); // 67

    System.out.println(birthDay.until(LocalDate.of(1967, 4, 8))); //P1M
    System.out.println(birthDay.until(LocalDate.of(1967, 4, 7))); //P30D
    System.out.println(birthDay.isAfter(LocalDate.of(1967, 4, 8))); //false

    LocalDate tomorrow1 = nowDate.plus(Period.ofDays(1));
    System.out.println(tomorrow1);

    System.out.println(tomorrow.until(nextMonth, ChronoUnit.DAYS)); // 30

  }

}