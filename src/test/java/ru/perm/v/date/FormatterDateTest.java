package ru.perm.v.date;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class FormatterDateTest {

  @Test
  @Ignore
  public void example() {
    LocalDateTime ddate = LocalDateTime.of(2019, 07, 21, 11, 20, 58, 0);
    assertEquals(DateTimeFormatter.ISO_DATE_TIME.format(ddate),
        ("2019-07-21T11:20:58")); // 2019-07-21T11:20:58
    assertTrue(DateTimeFormatter.ISO_DATE.format(ddate)
        .equals("2019-07-21")); // 2019-07-21

    OffsetDateTime zzd = ddate.atOffset(ZoneOffset.UTC);
    System.out.println(zzd);
    String strISO = DateTimeFormatter.ISO_DATE_TIME.format(zzd);
    System.out.println(strISO);
    strISO = "2019-07-21T11:00:58Z";
    LocalDateTime ldate = LocalDateTime
        .parse(strISO, DateTimeFormatter.ISO_DATE_TIME);
    System.out.println(ldate.atZone(ZoneOffset.UTC));
    System.out.println(
        DateTimeFormatter.ISO_INSTANT.format(ddate.toInstant(ZoneOffset.UTC)));
    DateTimeFormatter formatter = DateTimeFormatter
        .ofLocalizedDateTime(FormatStyle.LONG);
    ZonedDateTime zdate = ZonedDateTime.of(ddate, ZoneId.systemDefault());
    assertTrue(formatter.withLocale(Locale.getDefault()).format(zdate)
        .equals(
            "21 июля 2019 г. 11:20:58 YEKT")); // 21 июля 2019 г. 11:20:58 YEKT
    assertTrue(
        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).format(zdate)
            .equals("21.07.19 11:20")); // 21.07.19 11:20
    assertTrue(
        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(zdate)
            .equals("21.07.2019 11:20:58")); // 21.07.2019 11:20:58
//        System.out.println(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss").format(zdate));
    assertTrue(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss").format(zdate)
        .equals("2019/07/21 11:20:58"));

    LocalDate parseDate = LocalDate
        .parse("2019/07/22", DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    assertTrue(parseDate.equals(LocalDate.of(2019, Month.JULY, 22)));
  }

}
