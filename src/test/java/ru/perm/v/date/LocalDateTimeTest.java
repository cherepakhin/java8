package ru.perm.v.date;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;

public class LocalDateTimeTest {

  @Test
  public void example() {
    LocalDateTime now = LocalDateTime.now();
    System.out.println(now);
    LocalDateTime ddate = LocalDateTime.of(2019, 07, 21, 11, 20, 58);
    System.out.println(ddate.getDayOfMonth()); // 21
    System.out.println(ddate.getMonth()); // JULY
    System.out.println(ddate.getMonth().getValue()); // 7
    System.out.println(ddate.plusDays(1)); // 2019-07-22T11:20:58
    System.out.println(ddate.plus(Duration.ofDays(1))); // 2019-07-22T11:20:58
  }
}
